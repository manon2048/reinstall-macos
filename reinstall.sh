#!/bin/bash

# name			: reinstall.sh
# author(s)		: "echo" <dev@lointaine.fr>
# Creation date	: 2020-12-20
# Licence		: GPL v3 https://www.gnu.org/licenses/gpl-3.0.txt
# repository	: https://gitlab.com/echo314/ipcalc
# version		: 1.2
# Subject 		: outil de reinstall de mon mac

# installation de brew 
# /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# on creer un tableau 
declare -a t_logiciels=()

# bureautique 
t_logiciels+=("gimp")
t_logiciels+=("handbrake")
t_logiciels+=("libreoffice")
t_logiciels+=("libreoffice-language-pack")
t_logiciels+=("vlc")
t_logiciels+=("xnviewmp")

# dev
t_logiciels+=("anaconda")
t_logiciels+=("netbeans") 
t_logiciels+=("atom")
t_logiciels+=("virtualbox virtualbox-extension-pack")
t_logiciels+=("visual-paradigm-ce")
t_logiciels+=("brackets")
t_logiciels+=("mysqlworkbench")
t_logiciels+=("subethaedit")
t_logiciels+=("diffmerge")

# admin 
t_logiciels+=("fdupes")
t_logiciels+=("font-hack-nerd-font")
t_logiciels+=("fping")
t_logiciels+=("htop")
t_logiciels+=("nmap")
t_logiciels+=("wget")
t_logiciels+=("youtube-dl")

# internet 
t_logiciels+=("discord")
t_logiciels+=("firefox")
t_logiciels+=("thunderbird") 
t_logiciels+=("signal")
t_logiciels+=("telegram") 
t_logiciels+=("telegram-desktop")
t_logiciels+=("adium")
t_logiciels+=("skype")

# securite 
t_logiciels+=("keepassxc") 
t_logiciels+=("tunnelblick") 
t_logiciels+=("veracrypt") 
t_logiciels+=("gpg-suite") 
t_logiciels+=("tor-browser")

# utilitaire 
t_logiciels+=("android-file-transfer")
t_logiciels+=("balenaetcher")
t_logiciels+=("coconutbattery")
t_logiciels+=("onyx")
t_logiciels+=("unetbootin")
t_logiciels+=("install-disk-creator")
t_logiciels+=("transmission")
t_logiciels+=("intel-power-gadget")
t_logiciels+=("the-unarchiver")
t_logiciels+=("xquartz")

#jeu 
t_logiciels+=("steam") 

# calibre # en attente compatibilite DeDRM tools
# obs # pas sur de l’utiliser
# gpg-suite # a voir 

for i in "${!t_logiciels[@]}"
do	
	echo "telechargement de ${t_logiciels[$i]}"
	brew fetch ${t_logiciels[$i]}
done 

for i in "${!t_logiciels[@]}"
do	
	echo "installation de ${t_logiciels[$i]}"
	brew install ${t_logiciels[$i]}
done 
